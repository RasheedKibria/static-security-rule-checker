**Please download the latest release always as the tool often gets updated with new features. You will find the documentation of the tools inside the folder named "Documents".**

1. Tool Name: Static Security Rule Checker (S-SRC)

2. Target Users: RTL Design Engineers

3. Purpose: 
     - Detection of security-critical bugs present in the RTL design during code compilation
     - To make the RTL design engineers aware of potential security issues present in their RTL codes
     - Provide generic suggestions on how to fix the detected security-critical bugs
     - Assist to eliminate certain security bugs which are undetectable using simulation-based approaches
     - Identification of particular security-critical bugs even before performing Formal verification
     - Help to reduce the overall required time for security verification 

4. Supported Language for RTL Design: Verilog 2001

5. Operating System Requirement: Linux

6. Open-Source (Third-Party) Tool Dependencies:
   - Pyverilog (works as the front-end compiler for Verilog 2001): But you don't need to have it in your system.
   - iverilog (works as the pre-processor for the RTL codes written in Verilog 2001): You must have it installed in your system.
   - DOT (performs as the graph visualizer): You must have it installed in your system.

7. Contributors:
   - Rasheed Kibria
   - Dr. Farimah Farahmandi
   - Dr. Mark Tehranipoor

8. Contributors' Affiliation: University of Florida

9. Tool Developer, Architect, and Maintainer: Rasheed Kibria

10. Principal Investigators: 
    - Dr. Farimah Farahmandi
    - Dr. Mark Tehranipoor

11. Contact:
    - For technical queries: rasheed.kibria@ufl.edu
    - For other questions: farimah@ece.ufl.edu and tehranipoor@ece.ufl.edu

12. Future Support: Will provide support for SystemVerilog language when a good open-source compiler for SystemVerilog will be released (Similar to Pyverilog in terms of quality).

**N.B. Please use the provided RTL designs of the open-source benchmarks. These are compilable by Pyverilog and can be analyzed by our tools. I have fixed some quite minor compilation and coding issues which existed in the benchmarks. It does not change the functionality of the RTL designs. Synopsys Formality was used for the thorough testing process. You will be able to find the additional user inputs for the tools inside the folder named "USER_INPUTS". You have to copy and paste the contents in the appropriate fields of the provided GUI.**
